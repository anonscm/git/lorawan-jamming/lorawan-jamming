# LoRaWAN jamming

The LoRaWAN jamming repository aims at centralized research works and results about
modelisation and solutions against jamming attacks in LoRaWAN network.

The simulation and analytical post processing scripts are avalaible in this
repository.
It will allow to reproduce results in order to study performances
of LoRaWAN network under jamming attacks.

The analytical models are developed in a separate repository which contains implementation of analytical formulas
integrating jammer in LoRaWAN network.

The NS3 simulator is developed in a separate repository (https://sourcesup.renater.fr/projects/ns3-lorawan-jam)
The simulator allows to simulate several scenarios of LoRaWAN network under jamming.


The raw data of the results and the scripts to process data coming from
the analytical models, the ns3 simulator and the experiments are avalaible in
this repository.

A website is avalaible at https://sourcesup.renater.fr/www/lorawan-jamming/

