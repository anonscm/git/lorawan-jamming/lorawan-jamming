from lorawanjammod.models.jam import *

figure = 0
SF = 7
# Network User parameters
# --> Number of users
N_u = 50
# --> User Duty Cycle
d_u = float(0.01)
# -->  Maximum number allowed of re-transmissions
r = 5
PLA = 8 #ACK length

# Jammer parameters
# --> Numer of jammers
N_j = 50
# --> Aggregated normalised charge per jammer
charge_j = [x * 0.2 for x in range(0, 21)]

charge_j_eq = len(charge_j) * [[]]
for x in range(0, len(charge_j)):
    charge_j_eq[x] = charge_j[x] / N_j

print(charge_j_eq)
print(charge_j)

# Vector with different packet lengths
PLJ = [8, 16, 32]
# 1. Plot the Message Success Probability for the case where there is ACK channel and
# Jammers transmit on the uplink channel

Pm_up = len(charge_j_eq) * [[[]]]
Er_up = len(charge_j_eq) * [[[]]]
Th_up = len(charge_j_eq) * [[[]]]
Pm_dw = len(charge_j_eq) * [[[]]]
Er_dw = len(charge_j_eq) * [[[]]]
Th_dw = len(charge_j_eq) * [[[]]]

for x in range(0, len(PLJ)):
    #Pm_up[x], Er_up[x] = PlotJamUp(PLJ[x], N_j, d_u, charge_j_eq, N_u, r)
    Th_up[x], Pm_up[x], Er_up[x] = PlotJamUp(PLJ[x], N_j, d_u, charge_j_eq, N_u, r)
    Th_dw[x], Pm_dw[x], Er_dw[x] = PlotJamDw(PLJ[x], N_j, d_u, charge_j_eq, N_u, PLA, r)

figure = figure + 1
plt.figure(figure)

# Plotting the results, first part of the graph [from 10 user to 50]
Labels = list()
Text_labels = list()
UpDw = ['Only Up', 'Only Dw']

plt.figure(figsize=(4.5, 10))

plt.subplot(311)
for x in range(0, len(PLJ)):
    plt.plot(charge_j, Th_up[x], color=Colorlist[x], marker=m[x], markersize=5, fillstyle='none', linewidth=0.9, linestyle=l[0])
    Labels.append(mpatches.Patch(color=Colorlist[x], label='$l_j =$ ' + str(PLJ[x])))

#plt.figure(2)
for x in range(0, len(PLJ)):
    plt.plot(charge_j, Th_dw[x], color=Colorlist[x], marker=m[x], markersize=5, fillstyle='none', linewidth=0.9, linestyle=l[1])

for x in range(0, len(UpDw)):
    #Text_labels.append(mpatches.Patch(color=Colorlist[x], label='PLJ ' + UpDw[x]))
    Text_labels.append(mlines.Line2D([], [], color='Grey', markersize=5, fillstyle='none',  linestyle=l[x], label=UpDw[x]))


title = 'Mathematical Model (2)) \n Throughput \n  $r =$ ' + str(r) + ', $SF=$' + str(SF) + ', $d_u=$ ' + str(d_u) + ', $N_u=$ ' + str(N_u) + ', $N_j=$ ' + str(N_j)

plt.title(title)
plt.xlabel('Normalised traffic load of Jammers ($G_j = N_j \cdot \lambda_j \cdot T_j$)')
plt.ylabel('Throughput')
plt.grid(True)
plt.xlim([0, np.max(charge_j)])
plt.tight_layout()

legend = plt.legend(handles=Text_labels, loc=2, ncol=1, prop={'size': fontsize})
plt.gca().add_artist(legend)

legend1 = plt.legend(handles=Labels, loc=1, ncol=1, prop={'size': fontsize})
plt.gca().add_artist(legend1)


plt.subplot(312)

for x in range(0, len(PLJ)):
    plt.plot(charge_j, Pm_up[x], color=Colorlist[x], marker=m[x], markersize=5, fillstyle='none', linewidth=0.9, linestyle=l[0])
    Labels.append(mpatches.Patch(color=Colorlist[x], label='l_j = ' + str(PLJ[x])))

#plt.figure(2)
for x in range(0, len(PLJ)):
    plt.plot(charge_j, Pm_dw[x], color=Colorlist[x], marker=m[x], markersize=5, fillstyle='none', linewidth=0.9, linestyle=l[1])

title = 'Message Success Probability with re-transmissions \n  $r =$ ' + str(r) + ', SF=' + str(SF) + ', $d_u=$ ' + str(d_u) + ', $N_u=$ ' + str(N_u) + ', $N_j=$ ' + str(N_j)

plt.title(title)
plt.xlabel('Normalised traffic load of Jammers ($G_j = N_j \cdot \lambda_j \cdot T_j$)')
plt.ylabel('MSP')
plt.grid(True)
plt.xlim([0, np.max(charge_j)])
plt.tight_layout()



plt.subplot(313)
for x in range(0, len(PLJ)):
    plt.plot(charge_j, Er_up[x], color=Colorlist[x], marker=m[x], markersize=5, fillstyle='none', linewidth=0.9, linestyle=l[0])
    Labels.append(mpatches.Patch(color=Colorlist[x], label='l_j = ' + str(PLJ[x])))

#plt.figure(2)
for x in range(0, len(PLJ)):
    plt.plot(charge_j, Er_dw[x], color=Colorlist[x], marker=m[x], markersize=5, fillstyle='none', linewidth=0.9, linestyle=l[1])

title = 'Re-transmissions \n  $r = $' + str(r) + ', $SF=$' + str(SF) + ', $d_u=$ ' + str(d_u) + ', $N_u=$ ' + str(N_u) + ', $N_j=$ ' + str(N_j)

plt.title(title)
plt.xlabel('Normalised traffic load of Jammers ($G_j = N_j \cdot \lambda_j \cdot T_j$)')
plt.ylabel('Re-transmissions')
plt.grid(True)
plt.xlim([0, np.max(charge_j)])
plt.tight_layout()


plt.subplots_adjust(left=0.13, bottom=0.06, right=0.97, top=0.93, wspace=0.17, hspace=0.45)

plt.show()
