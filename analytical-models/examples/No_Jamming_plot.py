from lorawanjammod.models.jam import *

figure = 310

# Network parameters

d = 0.01
SF = 7
maxN = 451
granularity = 25
PL = 50
r = 1
N = np.arange(0, maxN, granularity)

## 1. Thoughput of an LoRaWAN Network without ACK nor Jamming

succes, charge = alohaplot (N,d,SF,PL)

figure = figure + 1

plt.subplot(figure)
plt.plot(N, succes, 'x-', color=Colorlist[0],markersize=5, linewidth=0.9)
Max_th = np.max(succes)
index_max = np.argmax(succes)
Max_g = charge[index_max]
print (N)
plt.axvline(x=N[index_max], color='red', linestyle ='--')
plt.text (N[index_max]*1.1, Max_th, 'G = ' + str(Max_g) + ', Th = '+ str(round(Max_th,4)))

title = 'Throughput ALOHA Pure, SF:  '+ str(SF) + ', d: ' + str(d) + ' $l_u$:' + str(PL)
plt.title(title)
plt.xlabel('Number of end-devices')
plt.ylabel('Normalised Throughput')
plt.xlim([0, maxN])
plt.grid(True)
plt.tight_layout()

label = list()
label.append(mpatches.Patch(color=Colorlist[0], label='r = 0'))
legend = plt.legend(handles=label, loc=1, ncol=1, prop={'size': fontsize})

plt.gca().add_artist(legend)

## 2. Message Succes Probability and average number of re-transmissions of an LoRaWAN Network with ACK and without Jamming

Retransmissions = [0, 4, 8, 16]

Pm = len(Retransmissions) * [[]]
Er = len(Retransmissions) * [[]]

# Scale of colors to plot
cmap = plt.cm.viridis
cmaplist = [cmap(i) for i in range(cmap.N)]
c = np.array(cmaplist)
cIdx = list(map(int, (np.arange(1, 100, granularity))))
c = c[cIdx]
label = list()

for x in [0, 1]:

    figure = figure + 1

    for y in range(0, len(Retransmissions)):
        Pm[y], Er[y] = plotgoodput(d, SF, maxN, granularity, Retransmissions[y])

        #Max_g[y] = charge[x][index_max[y]]

        #plt.axvline(x=N[0][index_max[x]], color='gray', linestyle='--')

        if x == 0:
            # plt.figure(figure)
            plt.subplot(figure)
            print(figure)
            plt.plot(N, Pm[y], color=Colorlist[y], marker=m[y],fillstyle='none',markersize=5, linewidth=0.9)
            label.append(mpatches.Patch(color=Colorlist[y], label='r = ' + str(Retransmissions[y])))
        elif x == 1:
            plt.subplot(figure)
            print(figure)
            #plt.figure(figure)
            plt.plot(N, Er[y], color=Colorlist[y],marker=m[y],fillstyle='none',markersize=5, linewidth=0.9)

    if x == 0:
        title = 'Message success probability' + '\n' + '$SF= $' + str(SF) + ', $d_u$: ' + str(d) + ' $l_u = $' + str(PL)
        plt.ylabel('Message success probability')
        legend = plt.legend(handles=label, loc=1, ncol=1, prop={'size': fontsize})
    elif x == 1:
        title = 'Expected number of retransmissions ' + '\n' + 'SF:' + str(SF) + ', $d_u$: ' + str(d) + ' $l_u = $' + str(PL)
                #+ '[sum (i=0 ^ r) [P D · (1 − P D ) ^ i ]] ' +'\n'
        plt.ylabel('Expected number of retransmissions per message')
        legend = plt.legend(handles=label, loc=1, ncol=1, prop={'size': fontsize})


    plt.title(title)
    plt.xlabel('Number of End Devices')
    plt.xlim([0, maxN])
    plt.grid(True)
    plt.tight_layout()
    plt.gca().add_artist(legend)


plt.figure(figsize=(4, 15))
plt.subplots_adjust(left=0.14, bottom=0.08, right=0.97, top=0.94, wspace=0.13, hspace=0.5)

plt.show()
