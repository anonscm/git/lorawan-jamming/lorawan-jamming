# README

Folder about the work on analytical models for LoRaWAN jamming.

In this directory you will find post-processing stuff related to the models
developed by Ivan Martinez as part of his thesis.


## Contributors and collaborations

This work is currently done by Ivan Martinez as part of his thesis supervised
by Fabienne Nouvel and Philippe Tanguy.

This work has been done in collaboration with Samer Lahoud (associate
professor) and Melhem El Helou (assistant professor), both working at the Falculty of
Engineerint ESIB at Saint Joseph University of Beirout.



