---
title: 'On the performance evaluation of LoRaWAN under Jamming'
subtitle: ''
summary: Evaluation of the performances of a LoRaWAN network under jamming attacks with the ns3 simulator.
authors:
- admin
tags:
- simulator
categories:
- performances
date: "2019-09-12T00:00:00Z"
lastmod: "2020-04-29T00:00:00Z"
featured: false
draft: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: ["ns3-lorawan-jamming"]
---

We presented in WMNC2020 our work about a performance evaluation of a LoRaWAN
network under jamming attacks.

This work has been done by Ivan Marino Martinez Bolivar as part of his PhD
thesis.

The analysis has been perform thanks to the ns3 simulator and a module
specially developed to study the behavior of the network when jammers are
present.

The source code of the ns3-lorawan-jamming module is open source and available at
[https://sourcesup.renater.fr/projects/ns3-lorawan-jam/](https://sourcesup.renater.fr/projects/ns3-lorawan-jam/).
The version of the code used for the results published in the conference article has been
tagged 'wmnc-2019' in the repository.
