---
# Display name
name: Philippe Tanguy

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Associate Professor

# Organizations/Affiliations
organizations:
- name: Lab-STICC
  url: "https://www.labsticc.fr/en/index/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include embedded system security, software defined-radio architecture and IoT system with a focus on the Cyber Security issues
---

Philippe Tanguy is associate professor at Université de Bretagne Sud (UBS). He
teach at the Université de Bretagne Sud in the UFR SSI. He performs his
research activities at Lab-STICC in the MOCS team. He had a PhD in Electronics
and digital communication at IETR. Currently, his research activities are
dedicated to IoT system with a focus on the Cyber Security issues.
