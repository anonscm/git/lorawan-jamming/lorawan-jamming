---
title: "On the performance evaluation of LoRaWAN under Jamming"
authors:
- Martinez Bolivar Ivan Marino
- Philippe Tanguy
- Fabienne Nouvel 
date: "2019-09-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2020-04-29T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *12th Wireless and Mobile Networking Conference*
publication_short: In *WMNC*

abstract: >
        In this paper we evaluate the performance of the LoRaWAN protocol under
        channel-aware and channel-oblivious jamming.  We obtained three key
        results, namely: (i) LoRaWAN networks are particularly vulnerable to
        jamming attacks, we have shown that the network throughput of the
        simulation scenarios chosen can be decreased by ∼ 56% when 25 jammers
        send unauthenticated packets permanently in the network, (ii) the
        gateway's performance is dramatically affected as a consequence of
        jammers. Our results suggest that the resources used to process packets
        coming from jammers could be 100 times higher than that of regular
        end-devices, and (iii) the network performance impact of jammers is
        highly correlated with the jammer class. We have shown that
        channel-oblivious jammers impacts the network performance widely, while
        channel-aware jammers impact the network locally. For this, we propose
        an ns-3 LoRaWAN module extension incorporating jamming attacks.

# Summary. An optional shortened abstract.
# summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
- WMNC
featured: false

links:
# - name: Custom Link
url_pdf: 'https://hal.archives-ouvertes.fr/hal-02301010'
url_code: 'https://sourcesup.renater.fr/projects/ns3-lorawan-jam/'
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
#image:
#  caption: ''
#  focal_point: ""
#  preview_only: false
#
# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- ns3-lorawan-jamming

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

